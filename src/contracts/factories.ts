import { OptimismL1FeeOracleAbi__factory, OffchainOracleAbi__factory, MulticallAbi__factory } from './';
import { optimismL1FeeOracleAddress, offchainOracleAddress, multiCallAddress } from '../config';
import { Provider } from '@ethersproject/abstract-provider';

export const getOptimismL1FeeOracle = (provider: Provider) => {
  return OptimismL1FeeOracleAbi__factory.connect(optimismL1FeeOracleAddress, provider);
};

export const getOffchainOracleContract = (provider: Provider) => {
  return OffchainOracleAbi__factory.connect(offchainOracleAddress, provider);
};

export const getMultiCallContract = (provider: Provider) => {
  return MulticallAbi__factory.connect(multiCallAddress, provider);
};
