import { GasPriceOracle } from '@tornado/gas-price-oracle';
import { BigNumber, BigNumberish, ethers } from 'ethers';
import BigNumberFloat from 'bignumber.js';
import { parseUnits } from 'ethers/lib/utils';
import {
  TransactionData,
  TxType,
  ITornadoFeeOracle,
  LegacyGasPriceKey,
  GasPriceParams,
  GetGasParamsRes,
  HexadecimalStringifiedNumber,
  GetGasInput,
  GetGasParamsInput,
} from './types';
import { JsonRpcProvider } from '@ethersproject/providers';
import { ChainId, defaultGasPrices, defaultInstanceTokensGasLimit, InstanceTokenSymbol } from './config';
import { bump, calculateGasPriceInWei, convertETHToToken, fromGweiToWeiHex, serializeTx } from './utils';
import { getOptimismL1FeeOracle } from './contracts/factories';
import { GetWithdrawalFeeViaRelayerInput } from './types';
import { AvailableTokenSymbols } from '@tornado/tornado-config';

export abstract class TornadoFeeOracle implements ITornadoFeeOracle {
  protected provider: JsonRpcProvider;

  public constructor(
    public version: 4 | 5,
    protected chainId: ChainId,
    rpcUrl: string,
    protected oracle: GasPriceOracle,
  ) {
    this.provider = new ethers.providers.JsonRpcProvider(rpcUrl);
  }

  /**
   * Because Optimism transaction published on Mainnet, for each OP transaction we need to calculate L1 security fee:
   * https://community.optimism.io/docs/developers/build/transaction-fees/#priority-fee
   * @param {TransactionData} [tx] Transaction data to estimate L1 additional fee
   * @returns {Promise<HexadecimalStringifiedNumber>} Fee in WEI (MATIC), '0' if chain is not Optimism
   */
  async fetchL1OptimismFee(tx?: TransactionData): Promise<HexadecimalStringifiedNumber> {
    if (this.chainId != ChainId.OPTIMISM) return BigNumber.from(0).toHexString();

    const optimismL1FeeOracle = getOptimismL1FeeOracle(this.provider);
    const l1Fee = await optimismL1FeeOracle.getL1Fee(serializeTx(tx));

    return l1Fee.toHexString();
  }

  /**
   * Estimate gas price, gas limit and l1Fee for sidechain (if exists)
   * @param {GetGasParamsInput} [params] Function input arguments object
   * @param {TransactionData} [params.tx] Transaction data in web3 / ethers format
   * @param {TxType} [params.txType=other] Tornado transaction type: withdrawal by user, withdrawal by relayer or 'other'
   * @param {number} [params.predefinedGasLimit] Predefined gas limit, if already calculated (no refetching)
   * @param {number} [params.predefinedGasPrice] Predefined gas price, if already calculated (no refetching)
   * @param {number} [params.bumpGasLimitPercent] Gas limit bump percent to prioritize transaction (if gas limit not predefined, recenlty used)
   * @param {number} [params.bumpGasPricePercent] Gas price bump percent to prioritize transaction (if gas limit not predefined, rarely used)
   * @param {LegacyGasPriceKey} [params.speed] Preferred transaction speed, if uses legacy gas (before EIP-1559)
   * @param {boolean} [params.includeL1FeeToGasLimit=true] Include L1 additional fee on Optimism to gas limit (get fee and divide by gas price)
   * @returns {Promise<GetGasParamsRes>} Object with fields 'gasPrice' and 'gasLimit', L1 fee, if exists, included in gasLimit
   */
  async getGasParams(params: GetGasParamsInput = {}): Promise<GetGasParamsRes> {
    let {
      tx,
      txType = 'other',
      bumpGasLimitPercent,
      bumpGasPricePercent,
      predefinedGasLimit: gasLimit,
      predefinedGasPrice: gasPrice,
      speed,
      includeL1FeeToGasLimit = true,
    } = params;

    let l1Fee: string = '0';
    if (!gasLimit && !gasPrice) {
      [gasPrice, gasLimit, l1Fee] = await Promise.all([
        this.getGasPrice(speed, bumpGasPricePercent),
        this.getGasLimit(tx, txType, bumpGasLimitPercent),
        this.fetchL1OptimismFee(tx),
      ]);
    }
    if (!gasLimit) {
      [gasLimit, l1Fee] = await Promise.all([
        this.getGasLimit(tx, txType, bumpGasLimitPercent),
        this.fetchL1OptimismFee(tx),
      ]);
    }
    if (!gasPrice) gasPrice = await this.getGasPrice(speed, bumpGasPricePercent);

    if (includeL1FeeToGasLimit)
      // Include L1 fee in gas limit (divide by gas price before), if l1 fee is 0, gas limit wont change
      gasLimit = BigNumberFloat(gasLimit)
        .plus(BigNumberFloat(l1Fee).div(BigNumberFloat(gasPrice)))
        .decimalPlaces(0, 1)
        .toNumber();

    return { gasLimit, gasPrice };
  }

  /**
   * Estimates next block gas for signed, unsigned or incomplete Tornado transaction
   * @param {GetGasInput} [params] Function input arguments object
   * @param {TransactionData} [params.tx] Transaction data in web3 / ethers format
   * @param {TxType} [params.txType] Tornado transaction type: withdrawal by user, withdrawal by relayer or 'other'
   * @param {number} [params.predefinedGasLimit] Predefined gas limit, if already calculated (no refetching)
   * @param {number} [params.predefinedGasPrice] Predefined gas price, if already calculated (no refetching)
   * @param {number} [params.bumpGasLimitPercent] Gas limit bump percent to prioritize transaction (if gas limit not predefined, recenlty used)
   * @param {number} [params.bumpGasPricePercent] Gas price bump percent to prioritize transaction (if gas price not predefined, rarely used)
   * @param {LegacyGasPriceKey} [params.speed] Preferred transaction speed, if uses legacy gas (before EIP-1559)
   * @returns {Promise<HexadecimalStringifiedNumber>} Gas value in WEI (hex-format)
   */
  async getGas(params: GetGasInput = {}): Promise<HexadecimalStringifiedNumber> {
    const { gasPrice, gasLimit } = await this.getGasParams({ ...params, includeL1FeeToGasLimit: true });

    return BigNumber.from(gasPrice).mul(gasLimit).toHexString();
  }

  /**
   * Estimate next block gas price
   * @param {LegacyGasPriceKey} [speed] Preferred transaction speed, if uses legacy gas (before EIP-1559)
   * @param {number} [bumpPercent=0] Gas bump percent to prioritize transaction
   * @returns {Promise<GasPriceParams>} Estimated gas price info in WEI (hexed) - legacy object with gasPrice property or
   * EIP-1559 object with maxFeePerGas and maxPriorityFeePerGas properties
   */
  async getGasPriceParams(speed?: LegacyGasPriceKey, bumpPercent: number = 0): Promise<GasPriceParams> {
    // Use instant for BSC, because on this chain "fast" and "instant" differs more than third and "fast" transaction can take hours
    if (!speed) speed = this.chainId === ChainId.BSC ? 'instant' : 'fast';
    try {
      return await this.oracle.getTxGasParams({ legacySpeed: speed, bumpPercent });
    } catch (e) {
      return { gasPrice: bump(fromGweiToWeiHex(defaultGasPrices[this.chainId][speed]), bumpPercent).toHexString() };
    }
  }

  /**
   * Estimate next block gas price
   * @param {LegacyGasPriceKey} [speed] Preferred transaction speed, if uses legacy gas (before EIP-1559)
   * @param {number} [bumpPercent] Gas bump percent to prioritize transaction
   * @returns {Promise<HexadecimalStringifiedNumber>} Gas price in WEI (hex string)
   */
  async getGasPrice(speed?: LegacyGasPriceKey, bumpPercent?: number): Promise<HexadecimalStringifiedNumber> {
    const gasPriceParams = await this.getGasPriceParams(speed, bumpPercent);
    return calculateGasPriceInWei(gasPriceParams).toHexString();
  }

  /**
   * Estimates gas limit for transaction (or basic gas limit, if no tx data provided)
   * @param {TransactionData} [tx] Transaction data (object in web3 / ethers format)
   * @param {TxType} [type] Tornado transaction type: withdrawal by user, withdrawal by relayer, relayer fee check or 'other'
   * @param {number} [bumpPercent] Gas bump percent to prioritize transaction
   * @returns {Promise<number>} Gas limit
   */
  abstract getGasLimit(tx?: TransactionData, type?: TxType, bumpPercent?: number): Promise<number>;

  /**
   * If user withdraw non-native tokens on ETH or Goerli, we need to calculate refund value:
   * if the withdrawal is successful, this amount will be returned to the user after the transfer to the relayer,
   * and if the relayer pays a commission and the transfer of tokens fails, this commission will remain to the relayer.
   *
   * Refund needed that recipient can use tokens after withdrawal (covers gas fee for send/swap)
   * @param {BigNumberish} gasPrice Actual gas price
   * @param {InstanceTokenSymbol} tokenSymbol Withdrawal token (currency) symbol - for example, 'dai'
   * @returns {HexadecimalStringifiedNumber} Refund amount in WEI (in hex format)
   */
  calculateRefundInETH(gasPrice: BigNumberish, tokenSymbol: InstanceTokenSymbol): HexadecimalStringifiedNumber {
    // Refund only available for non-native tokens on Ethereum Mainnet and Goerli
    if (![ChainId.MAINNET, ChainId.GOERLI].includes(this.chainId) || (tokenSymbol as AvailableTokenSymbols) === 'eth')
      return '0';

    // Notify user about error if incorrect token symbol provided
    if (!Object.values(InstanceTokenSymbol).includes(tokenSymbol)) {
      console.error(
        `Invalid token symbol: ${tokenSymbol}, must be lowercase token from one of Tornado ETH Mainnet pools`,
      );
      return '0';
    }

    // In Tornado we need to calculate refund only on user side, relayer get refund value in proof
    const gasLimit = defaultInstanceTokensGasLimit[tokenSymbol];
    return BigNumber.from(gasPrice).mul(gasLimit).mul(2).toHexString();
  }

  /**
   * Fetched actual gas price and calculates refund amount
   * @param {InstanceTokenSymbol} tokenSymbol Withdrawal token (currency) symbol - for example, 'dai'
   * @returns {Promise<HexadecimalStringifiedNumber>} Refund amount in WEI (in hex format)
   */
  async fetchRefundInETH(tokenSymbol: InstanceTokenSymbol): Promise<HexadecimalStringifiedNumber> {
    const gasPrice = await this.getGasPrice();
    return this.calculateRefundInETH(gasPrice, tokenSymbol);
  }

  /**
   * Get refund amount on ETH or Goerli in non-native token
   * @param {BigNumberish} gasPrice Actual gas price in ETH
   * @param {BigNumberish} tokenPriceInEth Token price in WEI in ETH
   * @param {HexadecimalStringifiedNumber | number} tokenDecimals Token (currency) decimals
   * @param {InstanceTokenSymbol} tokenSymbol Withdrawal token (currency) symbol - for example, 'dai'
   * @returns {HexadecimalStringifiedNumber} Refund amount in WEI in selected token (hexed number)
   */
  calculateRefundInToken(
    gasPrice: BigNumberish,
    tokenPriceInEth: BigNumberish,
    tokenDecimals: HexadecimalStringifiedNumber | number,
    tokenSymbol: InstanceTokenSymbol,
  ): HexadecimalStringifiedNumber {
    const refundInEth = this.calculateRefundInETH(gasPrice, tokenSymbol);
    return convertETHToToken(refundInEth, tokenDecimals, tokenPriceInEth).toHexString();
  }

  /**
   * Calculates relayer fee in selected currency (ETH, DAI, BNB etc) in WEI
   * @param {number | string} relayerFeePercent Relayer percent (0.4 for ETH Mainnet, for example)
   * @param {HexadecimalStringifiedNumber | number} amount Amount in selected currency (10 for 10 ETH, 1000 for 1000 DAI)
   * @param {string | number} decimals Decimal places in selected token (currency)
   * @returns {HexadecimalStringifiedNumber} Fee in WEI (hexed stingified number)
   */
  calculateRelayerFeeInWei(
    relayerFeePercent: number | string,
    amount: HexadecimalStringifiedNumber | number,
    decimals: string | number,
  ): HexadecimalStringifiedNumber {
    return parseUnits(amount.toString(), decimals)
      .mul(`${Math.floor(Number(relayerFeePercent) * 1e10)}`)
      .div(`${100 * 1e10}`)
      .toHexString();
  }

  /**
   * Estimates fee for withdrawal via relayer depending on type: gas bump percent is bigger, if it calculates by user,
   * so that the real commission from the relayer side is a little less,
   * in order to the relayer can send a transaction without fear that he will go into the red
   * @param {GetWithdrawalFeeViaRelayerInput} params Function input arguments object
   * @param {TxType} params.txType Tornado transaction type: withdrawal costs calculation from user side or from relayer side
   * @param {TransactionData} [params.tx] Transaction data (object in web3 / ethers format)
   * @param {number} params.relayerFeePercent Relayer fee percent from the transaction amount (for example, 0.15 for BNB or 0.4 for ETH Mainnet)
   * @param {AvailableTokenSymbols | Uppercase<AvailableTokenSymbols>} params.currency Currency symbol
   * @param {number | HexadecimalStringifiedNumber } params.amount Withdrawal amount in selected currency
   * @param {number | HexadecimalStringifiedNumber } params.decimals Token (currency) decimals
   * @param {BigNumberish} [params.refundInEth] Refund in ETH, if withdrawed other tokens on Mainnet (not ETH). Can not be provided, if user-side calculation
   * @param {BigNumberish} [params.tokenPriceInEth] If withdrawing other token on Mainnet or Goerli, need to provide token price in ETH (in WEI)
   * @param {number} [params.gasLimit] Predefined gas limit, if already calculated (no refetching)
   * @param {number} [params.gasPrice] Predefined gas price, if already calculated (no refetching)
   *
   * @returns {Promise<HexadecimalStringifiedNumber>} Fee in WEI (hexed string)
   */
  async calculateWithdrawalFeeViaRelayer({
    tx,
    txType,
    relayerFeePercent,
    currency,
    amount,
    decimals,
    refundInEth,
    tokenPriceInEth,
    predefinedGasLimit,
    predefinedGasPrice,
  }: GetWithdrawalFeeViaRelayerInput): Promise<HexadecimalStringifiedNumber> {
    const relayerFee = this.calculateRelayerFeeInWei(relayerFeePercent, amount, decimals);
    const { gasPrice, gasLimit } = await this.getGasParams({ tx, txType, predefinedGasLimit, predefinedGasPrice });
    const gasCosts = BigNumber.from(gasPrice).mul(gasLimit);

    if ((this.chainId === ChainId.MAINNET || this.chainId === ChainId.GOERLI) && currency.toLowerCase() != 'eth') {
      if (!tokenPriceInEth) {
        console.error('Token price is required argument, if not native chain token is withdrawed');
        return '0';
      }

      if (txType === 'user_withdrawal' && refundInEth === undefined)
        refundInEth = this.calculateRefundInETH(gasPrice, currency.toLowerCase() as InstanceTokenSymbol);

      const feeInEth = BigNumber.from(gasCosts).add(refundInEth || 0);
      return convertETHToToken(feeInEth, decimals, tokenPriceInEth).add(relayerFee).toHexString();
    }

    return BigNumber.from(gasCosts).add(relayerFee).toHexString();
  }
}
