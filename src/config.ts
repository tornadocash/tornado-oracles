import { LegacyGasPrices } from './types';

export enum ChainId {
  MAINNET = 1,
  GOERLI = 5,
  BSC = 56,
  XDAI = 100,
  POLYGON = 137,
  OPTIMISM = 10,
  ARBITRUM = 42161,
  AVAX = 43114,
}

export enum InstanceTokenSymbol {
  DAI = 'dai',
  cDAI = 'cdai',
  WBTC = 'wbtc',
  USDT = 'usdt',
  USDC = 'usdc',
}

export type GasPricesConfig = {
  [chainId in ChainId]: LegacyGasPrices;
};

export const defaultGasPrices: GasPricesConfig = {
  [ChainId.MAINNET]: {
    instant: 80,
    fast: 50,
    standard: 25,
    low: 8,
  },
  [ChainId.GOERLI]: {
    instant: 80,
    fast: 50,
    standard: 25,
    low: 8,
  },
  [ChainId.OPTIMISM]: {
    instant: 0.001,
    fast: 0.001,
    standard: 0.001,
    low: 0.001,
  },
  [ChainId.XDAI]: {
    instant: 6,
    fast: 5,
    standard: 4,
    low: 1,
  },
  [ChainId.BSC]: {
    instant: 5,
    fast: 4,
    standard: 3,
    low: 3,
  },
  [ChainId.POLYGON]: {
    instant: 100,
    fast: 75,
    standard: 50,
    low: 30,
  },
  [ChainId.ARBITRUM]: {
    instant: 4,
    fast: 3,
    standard: 2.52,
    low: 2.29,
  },
  [ChainId.AVAX]: {
    instant: 225,
    fast: 35,
    standard: 25,
    low: 25,
  },
};

type GasLimitConfig = {
  [chainId in ChainId]: number;
};

export const defaultWithdrawalGasLimit: GasLimitConfig = {
  [ChainId.MAINNET]: 550000,
  [ChainId.GOERLI]: 550000,
  [ChainId.ARBITRUM]: 1900000,
  [ChainId.OPTIMISM]: 440000,
  [ChainId.AVAX]: 390000,
  [ChainId.BSC]: 390000,
  [ChainId.POLYGON]: 390000,
  [ChainId.XDAI]: 390000,
};

type InstanceTokenGasLimitConfig = {
  [tokenSymbol in InstanceTokenSymbol]: number;
};

export const defaultInstanceTokensGasLimit: InstanceTokenGasLimitConfig = {
  [InstanceTokenSymbol.DAI]: 55000,
  [InstanceTokenSymbol.cDAI]: 425000,
  [InstanceTokenSymbol.WBTC]: 85000,
  [InstanceTokenSymbol.USDT]: 100000,
  [InstanceTokenSymbol.USDC]: 80000,
};

export const optimismL1FeeOracleAddress = '0x420000000000000000000000000000000000000F';
export const offchainOracleAddress = '0x07D91f5fb9Bf7798734C3f606dB065549F6893bb';
export const multiCallAddress = '0xda3c19c6fe954576707fa24695efb830d9cca1ca';
