import { BigNumberish, BytesLike } from 'ethers';
import { GasPriceKey, GetTxGasParamsRes } from '@tornado/gas-price-oracle/lib/services';
import { AvailableTokenSymbols } from '@tornado/tornado-config';
import { InstanceTokenSymbol } from './config';

// Type for big hexadecimal numbers, like 0x1eff87f47e37a0
export type HexadecimalStringifiedNumber = string;

export type LegacyGasPriceKey = GasPriceKey;
export type GasPriceParams = GetTxGasParamsRes;
export type LegacyGasPrices = {
  [gasPriceType in LegacyGasPriceKey]: number;
};

/* Tornado-specific transaction types:
  - 'user_withdrawal' - Fee calculation on user side, when user withdraw funds itself or via relayer on site
  - 'relayer_withdrawal' - Fee calculation on relayer side, when relayer sends withdrawal transaction
  - 'relayer_withdrawal_check_v4' - Fee calculation on relayer side, when V4 relayer checks user-provided fee. For compatibility reasons
  - 'other' - Any other non-specific transaction
*/
export type WithdrawalTxType = 'user_withdrawal' | 'relayer_withdrawal' | 'relayer_withdrawal_check_v4';
export type TxType = WithdrawalTxType | 'other';

export interface TransactionData {
  to: string;
  from?: string;
  nonce?: number;
  gasLimit?: BigNumberish;
  gasPrice?: BigNumberish;
  data?: string;
  value: BigNumberish;
  chainId?: number;
  type?: number;
  maxFeePerGas?: BigNumberish;
  maxPriorityFeePerGas?: BigNumberish;
}

export interface ITornadoFeeOracle {
  getGasParams: (params?: GetGasParamsInput) => Promise<GetGasParamsRes>;
  getGas: (params?: GetGasInput) => Promise<HexadecimalStringifiedNumber>;
  getGasPriceParams: (speed?: LegacyGasPriceKey, bumpPercent?: number) => Promise<GasPriceParams>;
  getGasPrice: (speed?: LegacyGasPriceKey, bumpPercent?: number) => Promise<HexadecimalStringifiedNumber>;
  getGasLimit: (tx?: TransactionData, type?: TxType, bumpPercent?: number) => Promise<number>;
  fetchL1OptimismFee: (tx?: TransactionData) => Promise<HexadecimalStringifiedNumber>;
  calculateRefundInETH: (gasPrice: BigNumberish, tokenSymbol: InstanceTokenSymbol) => HexadecimalStringifiedNumber;
  fetchRefundInETH: (tokenSymbol: InstanceTokenSymbol) => Promise<HexadecimalStringifiedNumber>;
  calculateRefundInToken: (
    gasPrice: BigNumberish,
    tokenPriceInEth: BigNumberish,
    tokenDecimals: HexadecimalStringifiedNumber | number,
    tokenSymbol: InstanceTokenSymbol,
  ) => HexadecimalStringifiedNumber;
  calculateWithdrawalFeeViaRelayer: (params: GetWithdrawalFeeViaRelayerInput) => Promise<HexadecimalStringifiedNumber>;
}

export interface ITornadoPriceOracle {
  defaultPrices: TokenPrices;
  fetchPrices: (tokens?: Token[]) => Promise<TokenPrices>;
}

export type WithdrawalData = {
  contract: string;
  proof: BytesLike;
  args: [BytesLike, BytesLike, string, string, BigNumberish, BigNumberish];
};
export type TornadoPoolInstance = {
  currency: AvailableTokenSymbols;
  amount: number;
  decimals: number;
};

// All non-native tokens from Tornado instances on ETH mainnet and TORN itself
export type TokenSymbol = 'dai' | 'cdai' | 'usdc' | 'usdt' | 'wbtc' | 'torn';
export type Token = {
  tokenAddress: string;
  symbol: TokenSymbol;
  decimals: number;
};
export type TokenPrices = { [tokenSymbol in TokenSymbol]?: BigNumberish };

// Reponse type for getGasParams function of fee oracle
export type GetGasParamsRes = {
  gasLimit: number;
  gasPrice: HexadecimalStringifiedNumber; // Gas price in native currency
};

export type GetGasInput = {
  // Transaction type: user-side calculation, relayer-side calculation or
  // relayer calculation to check user-provided fee in old V4 relayer (for backwards compatibility)
  txType?: TxType;
  tx?: TransactionData; // Transaction data in ethers format
  predefinedGasPrice?: HexadecimalStringifiedNumber; // Predefined gas price for withdrawal tx (wont be calculated again in function)
  predefinedGasLimit?: number; // Predefined gas limit for withdrawal tx (wont be calculated again in function)
  bumpGasLimitPercent?: number; // Gas limit bump percent to prioritize transaction (recenlty used)
  bumpGasPricePercent?: number; // Gas price bump percent to prioritize transaction (rarely used)
  speed?: LegacyGasPriceKey; // Preferred transaction speed, if uses legacy gas (before EIP-1559)
};

export type GetGasParamsInput = GetGasInput & { includeL1FeeToGasLimit?: boolean };

export type GetWithdrawalFeeViaRelayerInput = {
  // Transaction type: user-side calculation, relayer-side calculation or
  // relayer calculation to check user-provided fee in old V4 relayer (for backwards compatibility)
  txType: WithdrawalTxType;
  tx?: TransactionData; // Transaction data in ethers format
  relayerFeePercent: number | string; // Relayer fee percent from withdrawal amount (for example, 0.15 for BNB or 0.4 for ETH Mainnet)
  currency: AvailableTokenSymbols | Uppercase<AvailableTokenSymbols>; // Currency (token) symbol
  amount: string | number; // Withdrawal amount in selected currency (10 for 10 ETH, 10000 for 10000 DAI)
  decimals: string | number; // Token (currency) decimal places
  refundInEth?: HexadecimalStringifiedNumber; // Refund amount in ETH, if withdrawing non-native currency on ETH Mainnet or Goerli
  tokenPriceInEth?: HexadecimalStringifiedNumber | string; // Token (currency) price in ETH wei, if withdrawing non-native currency
  predefinedGasPrice?: HexadecimalStringifiedNumber; // Predefined gas price for withdrawal tx (wont be calculated again in function)
  predefinedGasLimit?: number; // Predefined gas limit for withdrawal tx (wont be calculated again in function)
};
