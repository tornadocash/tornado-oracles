import { MultiCall } from './contracts/MulticallAbi';
import { ITornadoPriceOracle, Token, TokenPrices, TokenSymbol } from './types';
import { MulticallAbi, OffchainOracleAbi } from './contracts';
import { getMultiCallContract, getOffchainOracleContract } from './contracts/factories';
import { Provider } from '@ethersproject/abstract-provider';
import { ethers, BigNumber } from 'ethers';
import { defaultAbiCoder } from 'ethers/lib/utils';
import { instances, Instances } from '@tornado/tornado-config';
import { ChainId } from './config';

/**
 * Filter non-native tokens from Tornado instances config
 * @param instances Tornado instances from torn-token config ('@tornado/tornado-config' library)
 * @returns Array of non-native tokens
 */
function filterTokensFromTornadoInstances(instances: Instances): Token[] {
  const ethInstances = Object.values(instances[ChainId.MAINNET]);
  return ethInstances.filter((tokenInstance) => !!tokenInstance.tokenAddress) as Token[];
}

const tornToken: Token = {
  tokenAddress: '0x77777FeDdddFfC19Ff86DB637967013e6C6A116C',
  symbol: 'torn',
  decimals: 18,
};
const tornadoTokens = [tornToken, ...filterTokensFromTornadoInstances(instances)];

const defaultTornadoTokenPrices: TokenPrices = {
  torn: '1689423546359032',
  dai: '598416104472725',
  cdai: '13384388487019',
  usdc: '599013776676721',
  usdt: '599323410893614',
  wbtc: '15659889148334216720',
};

export class TokenPriceOracle implements ITornadoPriceOracle {
  private oracle: OffchainOracleAbi;
  private multiCall: MulticallAbi;
  private provider: Provider;

  /**
   * Constructs TokenPriceOracle class instance
   * @param {string} rpcUrl http RPC (Ethereum Mainnet) url to fetch token prices from contract
   * @param {Token[]} [tokens] Array of tokens
   * @param {TokenPrices} [defaultTokenPrices] Default token prices, fallback if nothing loaded from contract
   */
  constructor(
    rpcUrl: string,
    private tokens: Token[] = tornadoTokens,
    private defaultTokenPrices: TokenPrices = defaultTornadoTokenPrices,
  ) {
    this.provider = new ethers.providers.JsonRpcProvider(rpcUrl);
    this.oracle = getOffchainOracleContract(this.provider);
    this.multiCall = getMultiCallContract(this.provider);
  }

  // Instant return default token prices
  get defaultPrices(): TokenPrices {
    return this.defaultTokenPrices;
  }

  /**
   * Prepare data for MultiCall contract
   * @param {Token[]} [tokens] Tokens array
   * @returns Valid structure to provide to MultiCall contract
   */
  private prepareCallData(tokens: Token[] = this.tokens): MultiCall.CallStruct[] {
    return tokens.map((token) => ({
      to: this.oracle.address,
      data: this.oracle.interface.encodeFunctionData('getRateToEth', [token.tokenAddress, true]),
    }));
  }

  /**
   * Fetch actual tokens price rate to ETH from offchain oracles
   * @param {Token[]} [tokens] Token array
   * @returns {TokenPrices} Object with token price rate to ETH in WEI
   */
  async fetchPrices(tokens: Token[] = this.tokens): Promise<TokenPrices> {
    try {
      if (!tokens?.length) return {};

      const callData = this.prepareCallData(tokens);
      const { results, success } = await this.multiCall.multicall(callData);
      const prices: TokenPrices = {};

      for (let i = 0; i < results.length; i++) {
        const tokenSymbol = tokens[i].symbol.toLowerCase() as TokenSymbol;
        if (!success[i]) {
          if (this.defaultTokenPrices[tokenSymbol]) prices[tokenSymbol] = this.defaultTokenPrices[tokenSymbol];
          continue;
        }

        const decodedRate = defaultAbiCoder.decode(['uint256'], results[i]).toString();
        const tokenDecimals = BigNumber.from(10).pow(tokens[i].decimals);
        const ethDecimals = BigNumber.from(10).pow(18);
        const price = BigNumber.from(decodedRate).mul(tokenDecimals).div(ethDecimals);
        prices[tokenSymbol] = price.toString();
      }
      return prices;
    } catch (e) {
      console.error('Cannot get token prices, return default: ' + e);
      return this.defaultTokenPrices;
    }
  }
}
