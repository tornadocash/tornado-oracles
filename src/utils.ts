import { serialize } from '@ethersproject/transactions';
import { GasPriceParams, TransactionData } from './types';
import { BigNumber, BigNumberish } from 'ethers';
import BigNumberFloat from 'bignumber.js';
BigNumberFloat.config({ EXPONENTIAL_AT: 100 });

const GWEI = 1e9;

export function serializeTx(tx?: TransactionData | string): string {
  if (!tx) tx = '0x';
  if (typeof tx === 'string') return tx;

  return serialize(tx);
}

export function calculateGasPriceInWei(gasPrice: GasPriceParams): BigNumber {
  // @ts-ignore
  return BigNumber.from(gasPrice.gasPrice || gasPrice.maxFeePerGas);
}

export function bump(value: BigNumberish, percent: number): BigNumber {
  const hundredPercents = BigNumberFloat(100);

  return BigNumber.from(
    BigNumberFloat(BigNumber.from(value).toHexString())
      .times(hundredPercents.plus(BigNumberFloat(percent)))
      .div(hundredPercents)
      .decimalPlaces(0, 1)
      .toString(),
  );
}

export function fromGweiToWeiHex(value: number | string): BigNumberish {
  return BigNumber.from(BigNumberFloat(value).times(GWEI).toString()).toHexString();
}

export function convertETHToToken(
  amountInWEI: BigNumberish,
  tokenDecimals: number | string,
  tokenPriceInWei: BigNumberish,
): BigNumber {
  const tokenDecimalsMultiplier = BigNumber.from(10).pow(tokenDecimals);
  return BigNumber.from(amountInWEI).mul(tokenDecimalsMultiplier).div(tokenPriceInWei);
}
