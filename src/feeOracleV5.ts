import { ChainId } from './config';
import { TornadoFeeOracle } from './feeOracle';
import {
  ITornadoFeeOracle,
  TransactionData,
  TxType,
  LegacyGasPrices,
  LegacyGasPriceKey,
  GasPriceParams,
} from './types';
import { GasPriceOracle } from '@tornado/gas-price-oracle';
import { bump } from './utils';
import { TornadoFeeOracleV4 } from './feeOracleV4';

/**
 * Oracle for new V5 version - estimates transaction fees with smart gas limit & bumping
 */
export class TornadoFeeOracleV5 extends TornadoFeeOracle implements ITornadoFeeOracle {
  private fallbackFeeOracle: TornadoFeeOracleV4;

  public constructor(chainId: number, rpcUrl: string, defaultGasPrices?: LegacyGasPrices) {
    const oracleConfig = {
      chainId,
      defaultRpc: rpcUrl,
      minPriority: chainId === ChainId.MAINNET || chainId === ChainId.GOERLI ? 2 : chainId === ChainId.BSC ? 3 : 0.05,
      percentile: 5,
      blocksCount: 20,
      defaultFallbackGasPrices: defaultGasPrices,
    };
    const gasPriceOracle = new GasPriceOracle(oracleConfig);

    super(5, chainId, rpcUrl, gasPriceOracle);

    this.fallbackFeeOracle = new TornadoFeeOracleV4(chainId, rpcUrl, defaultGasPrices);
  }

  async getGasLimit(tx?: TransactionData, type: TxType = 'other', bumpPercent: number = 20): Promise<number> {
    if (!tx || Object.keys(tx).length === 0) return this.fallbackFeeOracle.getGasLimit(tx, type, bumpPercent);

    /* Relayer gas limit must be lower so that fluctuations in gas price cannot lead to the fact that
     * the relayer will actually pay for gas more than the money allocated for this by the user
     * (that is, in fact, relayer will pay for gas from his own money, unless we make the bump percent less for him)
     */
    if (type === 'relayer_withdrawal') bumpPercent = 10;
    if (type === 'user_withdrawal') bumpPercent = 30;

    try {
      const fetchedGasLimit = await this.provider.estimateGas(tx);
      return bump(fetchedGasLimit, bumpPercent).toNumber();
    } catch (e) {
      return this.fallbackFeeOracle.getGasLimit(tx, type, bumpPercent);
    }
  }

  async getGasPriceParams(speed?: LegacyGasPriceKey, bumpPercent?: number): Promise<GasPriceParams> {
    // Only if bump percent didn't provided (if user provides 0, no need to recalculate)
    if (bumpPercent === undefined) {
      switch (this.chainId) {
        case ChainId.GOERLI:
          bumpPercent = 100;
          break;
        case ChainId.POLYGON:
        case ChainId.AVAX:
        case ChainId.XDAI:
          bumpPercent = 30;
          break;
        default:
          bumpPercent = 10;
      }
    }

    return super.getGasPriceParams(speed, bumpPercent);
  }
}
