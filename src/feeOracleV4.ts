import { defaultWithdrawalGasLimit } from './config';
import { TornadoFeeOracle } from './feeOracle';
import { ITornadoFeeOracle, TransactionData, TxType, LegacyGasPrices } from './types';
import { GasPriceOracle } from '@tornado/gas-price-oracle';
import { bump } from './utils';

/**
 * Oracle for V4 (old-version) transactions - estimates fee with predefined gas limit and without smart bumping
 */
export class TornadoFeeOracleV4 extends TornadoFeeOracle implements ITornadoFeeOracle {
  public constructor(chainId: number, rpcUrl: string, defaultGasPrices?: LegacyGasPrices) {
    const oracleConfig = {
      chainId,
      defaultRpc: rpcUrl,
      defaultFallbackGasPrices: defaultGasPrices,
    };
    const gasPriceOracle = new GasPriceOracle(oracleConfig);

    super(4, chainId, rpcUrl, gasPriceOracle);
  }

  async getGasLimit(tx?: TransactionData, type: TxType = 'other', bumpPercent: number = 0): Promise<number> {
    if (type === 'user_withdrawal') return bump(defaultWithdrawalGasLimit[this.chainId], bumpPercent).toNumber();

    // Need to bump relayer gas limit for transaction, because predefined gas limit to small to be 100% sure that transaction will be sent
    // This leads to fact that relayer often pays extra for gas from his own funds, however, this was designed by previous developers
    if (type === 'relayer_withdrawal')
      return bump(defaultWithdrawalGasLimit[this.chainId], bumpPercent || 25).toNumber();
    // For compatibility reasons, when wee check user-provided fee for V4 withdrawal transaction, we need dump gas limit
    // for about 20 percent,so that the transaction will be sent, even if it results in some loss for the relayer
    if (type === 'relayer_withdrawal_check_v4') return bump(defaultWithdrawalGasLimit[this.chainId], -25).toNumber();
    if (!tx || Object.keys(tx).length === 0) return bump(23_000, bumpPercent).toNumber();

    return bump(await this.provider.estimateGas(tx), bumpPercent).toNumber();
  }
}
