# Tornado oracles

This is a library providing convenient and fast access to oracles for Tornado-specific transactions, for example, withdrawal via relayer or getting rate to ETH for tokens that used in Tornado pools

### Installation

<hr>

- Create `.npmrc` file in project root with content `@tornado:registry=https://git.tornado.ws/api/packages/tornado-packages/npm/`
- Run `npm i @tornado/tornado-oracles`

### Import

<hr>

```typescript
const { TornadoFeeOracleV5, TornadoFeeOracleV5, TokenPriceOracle } = require('@tornado/tornado-oracles');
or
import { TornadoFeeOracleV5, TornadoFeeOracleV5, TokenPriceOracle } from '@tornado/tornado-oracles';
```

### Usage

<hr>

##### Estimate withdrawal gas costs

```typescript
import { TornadoFeeOracleV5 } from '@tornado/tornado-oracles';

const tx: TransactionData = {
  to: tornadoProxyLightAddress,
  data: poolInstance.methods.withdraw(...params).encodeABI(),
  value: withdrawalProofArgs[5],
};

const feeOracle = new TornadoFeeOracleV5(1, 'https://eth.llamarpc.com'); // First parameter - chain ID
const withdrawalGas = await feeOracle.getGas({tx, txType: 'relayer_withdrawal'});
```

##### Estimate gas price and gas limit to send transaction

```typescript
import { TornadoFeeOracleV5 } from '@tornado/tornado-oracles';

const incompleteTx: TransactionData = {
  to: tornadoProxyLightAddress,
  data: poolInstance.methods.withdraw(...params).encodeABI(),
  value: withdrawalProofArgs[5],
};

const feeOracle = new TornadoFeeOracleV5(1, 'https://eth.llamarpc.com');
const transactionType: TxType = 'relayer_withdrawal';
const { gasPrice, gasLimit } = await feeOracle.getGasParams({tx: incompleteTx, txType: transactionType});

const tx: TransactionData = {...incompleteTx, gasPrice, gasLimit}
```

##### Get token prices (rate to ETH) for tokens that used in Tornado

```typescript
import { TokenPriceOracle } from '@tornado/tornado-oracles';

const priceOracle = new TokenPriceOracle('https://eth.llamarpc.com');
const tokenPrices = await priceOracle.fetchPrices();

console.log(tokenPrices); // All prices in WEI
/* 
{
  torn: '1653773547906175', 
  dai: '603108348359886',
  cdai: '13487984643748',
  usdc: '601311723569085',
  usdt: '602058974373161',
  wbtc: '15696224089898846959'
}
*/
```

### License

[MIT](LICENSE)
